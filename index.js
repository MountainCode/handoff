// compare with `curl -v https://google.com`
const http = require('node:http');
const httpProxy = require('http-proxy');

const proxy = httpProxy.createProxyServer({});
const server = http.createServer((req, res) => {
  proxy.web(
    req,
    res,
    {
      target: 'https://google.com',
      changeOrigin: true
    },
  );
});

proxy.on('proxyRes', (proxyRes, req, res) => {
  console.log(proxyRes.constructor.name);
  console.log(proxyRes.headers);
});

const port = 3000;
console.log(`listenting on port ${port}`);
server.listen(port);
